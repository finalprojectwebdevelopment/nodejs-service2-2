const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const app = express();
const cors = require('cors');
//const configClient = require("node-config-client");
require('dotenv/config')

//import routes
const postsRoutes = require('./src/Routes/Create');

//Middleware
app.use(cors());
app.use(bodyparser.json());


//routes
app.use('/product/Create', postsRoutes);


//DbConnection
mongoose.connect(process.env.DB_connection, { useNewUrlParser: true, useUnifiedTopology: true });


//Running port
app.listen(3021);

// ------------------ Eureka Config --------------------------------------------
const Eureka = require('eureka-js-client').Eureka;

const eureka = new Eureka({
  instance: {
    app: 'nodejsrest2',
    hostName: 'localhost',
    ipAddr: 'ip',
    statusPageUrl: 'http://ip:3021',
    instanceId: 'ip:nodejsrest2:3021',
    status: 'true',
    port: {
      '$': 3021,
      '@enabled': 'true',
    },
    vipAddress: 'nodejsrest2',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    }
  },
  eureka: {
    registerWithEureka: true,
    fetchMetadata: true,
    host: 'localhost',
    port: 8585,
    servicePath: '/eureka/apps/',
    hostName: 'localhost'
  }
});
eureka.logger.level('debug');
eureka.start(function(error){
  console.log(error || 'complete');
});
